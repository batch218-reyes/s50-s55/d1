/*import button from  'react-bootstrap/Button';
import row from 'react-bootstrap/row';
import col from 'react-bootstrap/col';
import {Button, Row, Col} from 'react-bootstrap'
*/
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';



export default function Banner() {
return (
    <Row>
    	<Col className="p-5">
            <h1>Zuitt Coding Bootcamp</h1>
            <p>Opportunities for everyone, everywhere.</p>
            <Button variant="primary">Enroll now!</Button>
        </Col>
    </Row>
	)
}