import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Import the bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

/*
const name = "Ren Gabriel Reyes"; 
// JSX javascript XML
// with JSX we can apply js logic with HTML elements
const element =<h1>Hello, {name}</h1>
*/


/*const user = {
  firstName: 'Ren Gabriel',
  lastName: 'reyes'
}; 

function formatName(user) {
  return user.firstName + '' + user.lastName;
};

const element = <h1> Hello, {formatName(user)}</h1>
*/

