import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
// import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function LoginUser() {
    // Allows us to consume the user context object and properties to use for user validation
    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

// hook returns a function that lets us navigate to components
// const navigate = useNavigate();

    console.log(email);
     console.log(password);




    // Function to simulate redirection via form submission
    function logInUser (e) {
        //prevents page redirection via form submission
        e.preventDefault();
   fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
            } 
            else {
                    Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })

            };
        });

/*    // set the email fo the authenticated use in the local storage 
      localStorage.setItem('email', email);
        // sets the global user state to have properties obtained from the local storage
        setUser({email: localStorage.getItem('email')});

*/
        // Clear input fields
        setEmail("");
        setPassword("");
        //navigate('/');

/*        alert ('You are now logged in!');*/
    }

    const retrieveUserDetails = (token) => {
        fetch (`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation accross the whole application
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    };



    useEffect(() => {
        // validation to enable the submit button when all fields are populated and both passwods match.
        if (email !== '' && password !== ''){
            setIsActive(true);
        } else {
            setIsActive(false);
        }


    }, [email, password])

    return (

        (user.id != null) ?
        <Navigate to="/Courses"/>
        :

        <Form onSubmit={(e) => logInUser(e)}>
            <h2>Login</h2>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
                    value={password}
                    onChange={e => setPassword(e.target.value)}
	                required
                />
            </Form.Group>


                { isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
                 }


        </Form>
    )

}
